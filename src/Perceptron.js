const _ = require('lodash');
const Debugger = require('../util/Debugger');

const ACTIVATION_FUNCTIONS = {
    hs: function heavySide(n, th) {
        th = th || 0;
        if (n >= th) {
            return 1;
        }
        else {
            return 0;
        }
    },
    sig: function sigmoid(n) {
        return 1/(1+Math.pow(Math.E, -n));
    },
    reLu: function reLu(n) {
        return Math.max(0, n);
    },
    tanH: function tanH(n) {
        return Math.tanh(n);
    },
    sgn: function sign(n) {
        return Math.sign(n);
    }
};

const DEFAULT_OPTS = {
    inputLayers: 1,
    outputLayers: 1,
    randomWeights: true,
    activation: 'hs',
    debug: false
};

module.exports = class Perceptron {
    constructor(opts) {
        this.opts = _.assign(DEFAULT_OPTS, opts || {});
        this.debugger = new Debugger({activeLogs: this.opts.debug});
        this.inputNeuron = Array.from({length: this.opts.inputLayers});
        this.outputNeuron = Array.from({length: this.opts.outputLayers});
        this.bias = 1;

        this.weigths = this.initializeWeights();
    }

    setInputPattern(inputArray) {
        if (inputArray && inputArray.length > 0 && inputArray.length === this.opts.inputLayers) {
            for (let i = 0; i < this.opts.inputLayers; i++) {
                this.inputNeuron[i] = inputArray[i];
            }
            //Adding 1 input with value -1 for bias
            this.inputNeuron[this.opts.inputLayers] = this.bias;
            this.debugger.log('Bias:', this.inputNeuron[this.opts.inputLayers], 'SETTING INPUT:', this.inputNeuron);
        }
        else {
            throw new Error('Something is going wrong... InputLength is not the same as ');
        }

    }

    activationFunction(X) {
        this.debugger.log('SIGMA IS => ', X);

        return X.map((x, i) => {
            this.debugger.log('Calculating ', this.opts.activation, '( Sigma' + i + ' = ' + x + ')');
            return ACTIVATION_FUNCTIONS[this.opts.activation](x);
        });
    }

    sigma() {
        this.debugger.log('Calculating sigma with weights:', this.weigths);
        for (let j = 0; j < this.opts.outputLayers; j++) {
            let networkInput = 0;
            for (let i = 0; i < this.inputNeuron.length; i++) {
                networkInput = networkInput + this.weigths[j][i] * this.inputNeuron[i];
            }
            this.outputNeuron[j] = networkInput;
        }
        return this.outputNeuron;
    }

    initializeWeights() {
        return Array.from({length: this.opts.outputLayers}, () => {
            // + 1 to create a weight for bias too
            return Array.from({length: this.opts.inputLayers + 1}, () => {
                if (this.opts.randomWeights) {
                    return Math.random();
                }
                else {
                    return 0;
                }
            });
        });
    }

    propagate() {
        return this.activationFunction(this.sigma());
    }
};
const _ = require('lodash');
const Perceptron = require('./Perceptron');

const DEFAULT_OPTS = {
    trainingTimes: 5,
    learningRate: 0.5,
    debug: false,
    perceptronOpt: {
        inputLayers: 1,
        outputLayers: 1,
        randomWeights: true,
        activation: 'hs'
    }
};

module.exports = class Trainer {
    constructor(dataSet, opts) {
        this.opts = _.assign(DEFAULT_OPTS, opts || {});
        //injecting Debug options
        this.opts.perceptronOpt.debug = this.opts.debug;
        this.perceptron = new Perceptron(this.opts.perceptronOpt);

        //ToDo: controlling better the dataSet-Structure
        if (dataSet) {
            dataSet.map((row) => {
                //TODO: Implement better type control etc.
                if (!(row && row.input && row.output &&
                    (row.input.length === this.opts.perceptronOpt.inputLayers
                        && row.output.length === this.opts.perceptronOpt.outputLayers))) {
                    throw new Error('Something is going wrong with your Inputs/Outputs in Data-Set!');
                }
            });
            this.dataSet = dataSet;
        }
        else {
            throw new Error('Something is going wrong in the Data-Set');
        }
    }

    calculateSingleWeight(weightN, inputN, outputN, calculatedOutputN) {
        // following function describes what I'm going to implement
        // Wn = Wn + learningRate * Xn * (realOutput - calculatedOutput)
        return weightN + this.opts.learningRate * inputN * (outputN - calculatedOutputN);

    }

    activate(input) {
        //Todo: implement better control on types
        if (input && input.length === this.perceptron.opts.inputLayers) {
            this.perceptron.setInputPattern(input);
            return this.perceptron.propagate();
        }
        else {
            throw new Error('Need array');
        }
    }

    async train() {
        this.perceptron.debugger.log('TRAININIG Simple-PERCEPTRON', this.opts.trainingTimes, 'times');
        for (let j = 0; j < this.opts.trainingTimes; j++) {
            this.perceptron.debugger.log('\nTRAIN NR:', j + 1, '\n');
            for (let i = 0; i < this.dataSet.length; i++) {
                this.perceptron.debugger.log('_____________TEST ROW ' + (i + 1) + '______________');
                this.perceptron.debugger.log('TESTING:', this.dataSet[i].input, '=>', this.dataSet[i].output);

                this.perceptron.setInputPattern(this.dataSet[i].input);
                let result = this.perceptron.propagate();
                this.perceptron.debugger.log('RESULT IS:', result);

                for (let o = 0; o < this.perceptron.opts.outputLayers; o++) {
                    for (let inp = 0; inp < this.perceptron.inputNeuron.length; inp++) {
                        this.perceptron.weigths[o][inp] = this.calculateSingleWeight(this.perceptron.weigths[o][inp],
                            this.perceptron.inputNeuron[inp],
                            this.dataSet[i].output[o],
                            result[o]);
                    }
                }
                this.perceptron.debugger.log('____________________________________');
            }
        }
    }
};
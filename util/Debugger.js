const DEFAULT_OPTIONS = {
    activeLogs: false
};

module.exports = class Debugger {
    constructor(options) {
        let self = this;
        self.options = options || DEFAULT_OPTIONS;
    }

    log() {
        let self = this;
        if (self.options.activeLogs && arguments && arguments.length > 0) {
            console.log.apply(console, arguments);
        }
    }

    //Will logs ever without looking options
    warning() {
        //TODO: Find a way to make string of arguments colorful
        const BG_RED = '\x1b[41m';
        const BLINK = '\x1b[5m';
        console.error.apply(console, arguments);
    }

};
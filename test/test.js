const SETS = require('./DATA_SET');
const {Trainer} = require('../index');

let funcNames = ['OR', 'AND', 'NOT', 'ACTIONS', 'XOR'];
let funcName = funcNames[1];

let dataSet = SETS[funcName];

const trainerOpts = {
    perceptronOpt: {
        inputLayers: dataSet[0].input.length,
        outputLayers: dataSet[0].output.length,
        randomWeights: true,
        activation: 'hs'
    },
    debug: false,
    trainingTimes: 50
};

let t = new Trainer(dataSet, trainerOpts);

console.log('\n______DATASET________');
console.log(JSON.stringify(dataSet, null, 0));
console.log('_____________________\n');

t.train().then(() => {
    console.log('Trained', trainerOpts.trainingTimes, 'times\n');
    return dataSet.map((row, i) => {
        console.log('TESTING ROW:', i, '=>', row);
        console.log('Input was', row.input, ' - RES:', t.activate(row.input));
    });
});
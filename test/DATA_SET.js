const DATA_SET = {
    // DataSet is a simple OR-Function
    OR: [
        {input: [0, 0], output: [0]},
        {input: [1, 0], output: [1]},
        {input: [0, 1], output: [1]},
        {input: [1, 1], output: [1]}
    ],
    // DataSet is a simple AND-Function
    AND: [
        {input: [0, 0], output: [0]},
        {input: [1, 0], output: [0]},
        {input: [0, 1], output: [0]},
        {input: [1, 1], output: [1]}
    ],
    // DataSet is a simple NOT-Function
    NOT: [
        {input: [0], output: [1]},
        {input: [1], output: [0]}
    ],
    // DataSet is a function with actions
    ACTIONS: [
        {input: [0, 0], output: [0, 0, 0]}, // Nothing
        {input: [1, 0], output: [1, 0, 0]}, // First action
        {input: [0, 1], output: [0, 1, 0]}, // Second action
        {input: [1, 1], output: [0, 0, 1]}  // Third action
    ],
    // DataSet is a not linear XOR-Function
    XOR: [
        {input: [0, 0], output: [0]},
        {input: [1, 0], output: [1]},
        {input: [0, 1], output: [1]},
        {input: [1, 1], output: [0]}
    ]
};

module.exports = DATA_SET;
# myPerceptron

### INTRO

myPerceptron is a Javascript-library for nodejs-projects, complete written in pure EcmaScript6, to understand how does a SimplePerceptron work.

### HOW TO TEST

You can set at begin a data-set including a linear Problem that will be solved after some train times.

- DataSet is a simple OR-Function
 ```javascript
const dataSet = [
    {input: [0, 0], output: [0]},
    {input: [1, 0], output: [1]},
    {input: [0, 1], output: [1]},
    {input: [1, 1], output: [1]}
];
```

- DataSet is a simple AND-Function
```javascript
 const dataSet = [
     {input: [0, 0], output: [0]},
     {input: [1, 0], output: [0]},
     {input: [0, 1], output: [0]},
     {input: [1, 1], output: [1]}
 ];
```

- DataSet is a simple NOT-Function
```javascript
 const dataSet = [
     {input: [0], output: [1]},
     {input: [1], output: [0]}
 ];
```

-  DataSet is a function with actions to trigger
```javascript
 const dataSet = [
     {input: [0, 0], output: [0, 0, 0]},  //Nothing
     {input: [1, 0], output: [1, 0, 0]},  //First action
     {input: [0, 1], output: [0, 1, 0]},  //Second action
     {input: [1, 1], output: [0, 0, 1]}   //Third action
 ];
```

Once you set the data-set, than you can

```javascript
const {Trainer} = require('./index');

const trainerOpts = {
    perceptronOpt: {
        inputLayers: dataSet[0].input.length,
        outputLayers: dataSet[0].output.length,
        randomWeights: true,
        activation: 'hs'
    }, 
    debug: false, 
    trainingTimes: 50
};

let t = new Trainer(dataSet, trainerOpts);

return t.train().then(() => {
    console.log('Trained', trainerOpts.trainingTimes, 'times\n');
    return dataSet.map((row, i) => {
        console.log('TESTING ROW:', i, '=>', row);
        console.log('Input was', row.input, ' - RES:', t.activate(row.input));
    });
});
```

##### Output will be something so:

```console
______DATASET________
[{"input":[0,0],"output":[0]},{"input":[1,0],"output":[0]},{"input":[0,1],"output":[0]},{"input":[1,1],"output":[1]}]
_____________________

Trained 50 times

TESTING ROW: 0 => { input: [ 0, 0 ], output: [ 0 ] }
Input was [ 0, 0 ]  - RES: [ 0 ]
TESTING ROW: 1 => { input: [ 1, 0 ], output: [ 0 ] }
Input was [ 1, 0 ]  - RES: [ 0 ]
TESTING ROW: 2 => { input: [ 0, 1 ], output: [ 0 ] }
Input was [ 0, 1 ]  - RES: [ 0 ]
TESTING ROW: 3 => { input: [ 1, 1 ], output: [ 1 ] }
Input was [ 1, 1 ]  - RES: [ 1 ]
```

### Documentation

This will come soon...
const EXPORT = {
    Perceptron: require('./src/Perceptron'),
    Trainer: require('./src/Trainer')
};

module.exports = EXPORT;
